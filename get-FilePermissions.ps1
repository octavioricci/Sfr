## get-FilePermissions
function get-FilePermissions{

    [CmdletBinding()]
    param(# Parameter help description
    [Parameter(Mandatory=$true)]
    [ValidateScript({test-path -path $_})]
    [String]$path
    )

    BEGIN{
        $outputPreference = "continue"
        $array=@()
        
    }

    PROCESS{

            $acl = get-acl $path 
            
                   
            $acl.Access | % {
        
                #$grupo=$_.identityreference
                
                switch ($_) {
                   {(($_.InheritanceFlags -eq "none") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}     
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This Folder Only"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This Folder Only"
                        }

                   {(($_.InheritanceFlags -eq "ContainerInherit, ObjectInherit") -and `
                                                                ($_.PropagationFlags -eq "none") -and ($_.accesscontroltype -eq "allow"))}
                        {
                            
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder, sub-folders and files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This Folder Only"
                        
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder, sub-folders and files"
                        }

                    {(($_.InheritanceFlags -eq "ContainerInherit") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder and sub-folders"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder and sub-folders"
                        }
                    {(($_.InheritanceFlags -eq "ObjectInherit") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder and files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder and files"
                        }
                    {(($_.InheritanceFlags -eq "ContainerInherit, ObjectInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Subfolders and file"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Subfolders and files"
                        }
                    {(($_.InheritanceFlags -eq "ContainerInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Subfolders"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Subfolders"
                        }
                    {(($_.InheritanceFlags -eq "ObjectInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and`
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Files"
                        }           
                   Default {}        
                } #cierre switch
      
            }# Cierre foreach 
        Write-Output $array
        }# cierre PROCESS
           
}# cierre Function

