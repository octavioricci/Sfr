## Get-CredentialInput
function get-CredentialInput{

    [CmdLetBinding()]
    param(
    [Parameter(Mandatory=$false)]
    [ValidateSet('nbk9qaus','roj48598s','nbkrqt8s','lfra2477s','rhr87744s','bm113150s','fa110791s', 
                'qv110627s','sg115382s','secguard')]
    [ValidateScript({

                try {
                    $user=$_
                    get-aduser -Identity $user -ErrorAction stop
                    $true
                }
                catch {
                    throw "No existe el usuario $user"
                }

    })]
    [String]$user,
    [Parameter(Mandatory=$false)]
    [String]$password
    )

   

    BEGIN{
        
        $domain="ardp"
        # Si no ingresé usuario por parámetro
        if(!$user){
            $Puser=Read-Host "Ingrese usuario "
            
        }
        if(!$password){
        
            $Ppassword=Read-Host "Ingrese password " -AsSecureString
            #$credential=New-Object System.Management.Automation.PSCredential("$domain\$user",$password)
        }
        elseif ($user -and $password){
            $Puser=$user
            $Ppassword = $password | ConvertTo-SecureString -AsPlainText -Force

        }    
        

     
      
      $credential=New-Object System.Management.Automation.PSCredential("$domain\$Puser",$Ppassword)

    }

    PROCESS{}


    END{
        return $credential
    }

}


