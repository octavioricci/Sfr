function get-stringLength(){
	
    [CmdLetBinding()]
    param(
        [parameter(Mandatory=$true)]
        [String]$path

    )
	
    BEGIN{}

    PROCESS{

        $cant=$path | Measure-Object -Character
        $cantidad=$cant.Characters
        return $cantidad
    }
	


}