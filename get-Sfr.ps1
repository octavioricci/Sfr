## get-Sfr
function get-Sfr{



    
    [CmdLetBinding()]
    param(
        [parameter(Mandatory=$true)]
        #[validateScript({
        #   if(-not (Test-Connection $_ -Count 1 -Quiet)){
        #    throw "No se encuentra el server solicitado"
        #   }
        #   else{
        #       $True
        #   }
       # })] # Cierre validateScript
        [String]$server,
        [parameter(Mandatory=$false)]
        [int]$depth

        
    )
    
    BEGIN {
        #$VerbosePreference = "continue"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\get-StringLength[OK].ps1" 
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\get-FilePermissions[OK].ps1"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\Get-HddDrives[OK].ps1"
        #. "C:\Temp\PF\Proyecto FileServer\Get-CredentialInput[OK].ps1"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\Get-CredentialInput[OK].ps1"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\Get-AclLocaly[OK].ps1"
        
        # Esto significa que las exceptions de tipo "nonterminating", las transformo en "terminating", lo que posibilita
        # que pueda tratarlas con try/catch
        # NOTA: Nunca hacer $ErrorActionPreference = 'SilentlyContinue' .
        #       Porque continua ante los errores nonterminating y de esta manera no nos enteraríamos de los errores
        $ErrorActionPreference="stop"
        $array=@()
        $errorLog=@()
        $permissions=@{}
        $driveArray=@()
        $folderCountArray=@()
        $ht=@{}
        $date=Get-Date -Format "dd-MM-yyyy_HHmm"
        $ARCHIVO="c:\temp\permisos_$server"+"_$date.csv"
        
        ## Primero Valido que el server exista y que la conexión remota esté habilitada
        #if (-not (get-TestPsRemoting $server)){
        #    return
        #}

        # Valido la clave
        try {
            # Obtengo la credencial
            $cred=Get-CredentialInput
            #creo Session
            $session=New-PSSession -ComputerName $server -Credential $cred -ErrorAction stop
            
            
        
        }catch{
            switch -wildcard ($_.FullyQualifiedErrorId) {
                
                'CannotConnect,PSSessionOpenFailed'{
                    Write-host "Hubo un problema en la validacion, verifique que el servicio winrm esté levantado" -ForegroundColor black -BackgroundColor yellow
                    #pause
                    #exit 0
                    
                }
                
                'AccessDenied,PSSessionOpenFailed*'{
                    Write-host "No posee los permisos para acceder al server" -ForegroundColor black -BackgroundColor yellow
                    #Pause
                    #exit 0
                    
                }    
                
            }
        }


        
        }#CIERRE BEGIN
    
    
    PROCESS {

       
        
        

        
    
        # Si no obtuve sesion termina
        if ($session){

                    # CREO EXCEL
                    Write-Output "Directory`tSecurity" >> $ARCHIVO
                    
                    # Obtengo drives del server
                    $drives=get-HddDrives $server $cred

                   
                    # Por cada drive
                    foreach($drive in $drives){
                        
                        $j=0

                        #$folderCount,$folders,$errorLog=Invoke-Command -Session $session -ScriptBlock ${function:get-AclLocally} -args $drive #-Credential $cred
                        #Write-Output "Voy a pasar los parametros $drive y $depth, que es de tipo $($depth.gettype())"
                        $results=Invoke-Command -Session $session -ScriptBlock ${function:get-AclLocally} -args $drive,$depth #-Credential $cred
                       

                        Write-Host "Cantidad de Folders= $($results.CountFolders)" -ForegroundColor black -BackgroundColor yellow
                        
                        $folderCount= $($results.CountFolders)
                                                           
                        $array+=@{
                            Directorio=$drive
                            CantidadFolders=$results.CountFolders
                        }

                        #Write-Output "Cantidad directorios: $($results.CountFolders)"
                        #Write-Output "Directorios: $($results.TotalFolders)"
                        #Write-Output "Errores: $($results.ErrorVariable)"
                                                
                        # por cada folder
                        foreach($folder in $($results.TotalFolders)){

                            Write-Host "Folder : $folder" -ForegroundColor Red
                            
                            #$folderFinal+=$folder
                            Write-Progress -Activity "Escaneando en directorios..." -CurrentOperation "Leyendo $folder" `
                            -Status "Por favor espere....[$j]/[$folderCount]" -PercentComplete ($j/$folderCount*100)

                                $permissions=Invoke-Command -Session $session -ScriptBlock ${function:get-FilePermissions} -args $folder #-Credential $cred
                                Write-Output "$folder"
                                #$permissions=get-FilePermissions $folder

                            # Agrego en Excel los permisos
                            for($i=0;$i -lt $permissions.count;$i++){
                            
                                        
                                        Write-Output "$folder`t$($permissions[$i])" >> $ARCHIVO
                                      
                            }
                            
                            $j++
                        }
                        
                      
                       
                       
                    } # Cierre ForEach drives     
                   

                <#        
                        
                        $errorLog=@()
                        Write-Progress -Activity "Leyendo Directorios..." -Status $path -PercentComplete 0 
                                                
                        # Creo array de carpeta 
                        $folders=@()

                        # Obtengo nombre de la carpeta que pase por parametro
                        $folders +=Get-Item $path | Select-Object -ExpandProperty FullName
                                                
                        # Obtengo subdirectorios de la carpeta madre
                        $subfolders = Get-ChildItem $path -Recurse -ErrorVariable +ErrorLog `
                        -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                        Select-Object -ExpandProperty FullName
                        Write-Progress -Activity "Leyendo Directorios..." -Status $path -PercentComplete 100

                        # Validamos que tenga subdirectorios para evitar nulos
                        if($subfolders){

                            # junto todo en la variable folders
                            $folders += $subfolders
                        }          
                                                
                        #contabilizo si tiene subfolder
                        $row=2
                        # Va a ser el contandor de las carpetas
                        $j=0
                        $folderCount=$folders.count
                        write-output "Cantidad de directorios $folderCount"
                                                    
                                                    

                        # Leo todos los directorios recursivamente
                        foreach ($folder in $folders){

                                                    
                        Write-Progress -Activity "Escaneando en directorios..." -CurrentOperation "Leyendo $folder" `
                            -Status "Por favor espere....[$j]/[$folderCount]" -PercentComplete ($j/$folderCount*100)
                                                    
                                                            

                        #Leo carpeta por carpeta los permisos
                        try{
                            # Obtengo la cantidad de caracteres
                            $cantidad=get-StringLength($folder)
                            #seteo la cantidad de - según la cantidad de caracteres del $folder
                            $str="-"*($cantidad+11)
                                                            
                                                            
                            $permissions=get-FilePermissions $folder
                                                        

                            #Write-Output "Cantidad de elementos $($permissions.count)"
                            #Write-output "`n[Carpeta];$folder;`t$permissions" 
                            $acl = Get-Acl -LiteralPath $folder -ErrorAction Continue
                                                            
                            # Agrego en Excel los permisos
                                                            
                            for($i=1;$i -lt $permissions.count;$i++){
                            $c.cells.item($row,$columnName) = $folder
                            $c.cells.item($row,$columnSecurity) = $permissions[$i]
                            $row++
                            }
                                                            
                                                            

                        }catch{
                            # Creo un custom object
                            $errorLog += New-Object -TypeName psobject -Property @{ErrorType=$_CategoryInfo;TargetObject=$folder}
                        }
                                        
                            $j++
                                                
                                                
                                
                                # Voy mostrando los permisos sobre cada carpeta
                                $acl.Access | ? {$_.IsInherited  -eq $false} | 
                                Select-Object `
                                @{n='Root';e={$path}},`
                                @{n='Path';e={$folder}},`
                                @{n='Group or User';e={$_.identityreference}},`
                                @{n='Permissions';e={$_.filesystemrights}},`
                                @{n='Inherited';e={$_.isinherited}},`
                                inheritanceFlags,propagationflags
                                    
                                                    
                                                    
                                                    

                        } # cierre foreach


                    }


                
                    # Agrego Titulo
                    $c.cells.item(1,$columnTitleName) = "Directory Name"
                    $c.cells.item(1,$columnTitleSecurity) = "Security"
                    $b.SaveAs("c:\temp\permisos.xls")
                    $a.quit()

                     #>

                    # Exporto los errores que vengo capturando en la obtencion de subdirectorios y acls
                                        #$errorLog | Select-Object ErrorType, TargetObject | Export-Csv "c:\temp\filepermission.csv" -NoTypeInformation
                    #Write-Output "CARPETA:$($errorlog.folders)`t`tERROR:$($errorlog.ErrorVariable.exception)"
                    
                    # Por último guardo el resumen de drives y su cantidad de directorios
                    foreach($arr in $array){
                        Write-Output "Drive=$($arr.Directorio)`tCantidad Directorios=$($arr.CantidadFolders)" >> $ARCHIVO
                    }
                    

                    $results.ErrorVariable | Select-Object TargetObject,CategoryInfo | Export-Csv "c:\temp\filepermissionErrors.csv" -NoTypeInformation
                     



        }# Cierre IF $session
         # Si no tuve sesion, se termina
    }# cierre PROCESS

}


