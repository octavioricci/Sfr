## Get-AclLocaly
function Get-AclLocally {

    [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [String]$drive,
        [Parameter(Mandatory=$false)]
        [int]$depth 

    )

    BEGIN{
        #$err=@()
        $simbolo='*\'
        $dirArray=@()
        # Creo array de carpeta 
        $folders=@()
        # Creo array de subcarpeta
        $subfolders=@()
        $ultimaLetra=$drive.Substring($drive.length-1)
        
        #Si la ultima letra no contiene '\' se la agrega
        if(-not ($ultimaLetra.Contains(('\')))){
            $drive+='\'
        }
        
    }
            

    PROCESS {
               
               Write-Progress -Activity "Leyendo Directorios..." -Status $drive -PercentComplete 0 
                                    
               
               

               # Obtengo nombre de la carpeta que pase por parametro
               $folders +=Get-Item $drive | Select-Object -ExpandProperty FullName

               # Si recibe profundidad de subdirectorios                     
               if ($depth){

                    $tempDepth=""

                    # Por cada profundidad
                    foreach ($i in 1..$depth){
                        
                        
                        $tempDepth+=$simbolo
                        $dirArray+=$tempDepth   

                       
                    }                

                    # En dirArray tengo el string del depth (ej: "\\\ 3 niveles"),
                    # o sea, tengo la cantidad de niveles que tengo que recorrer
                    foreach ($j in $dirArray){

                        
                        Write-Output "drive= $($drive+$j)"
                        # Por cada directorio que recorro (ej: d:\*\). pregunto si tiene subdirectorios
                        # Si tiene subdirectorios, entonces listo los subdirectorios con los permisos
                        # Sino, break
                        # Esto es para 
                        if (Get-ChildItem $drive$j -ErrorAction SilentlyContinue -ErrorVariable +err | 
                            ? {$_.psiscontainer -eq $true})
                            {
                                    $subfolders+= Get-ChildItem $drive$j -ErrorVariable +err `
                                    -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                                    Select-Object -ExpandProperty FullName
                            }
                          else{
                              break
                          }  
                        
                    }
                    

               }
               elseif (-not $depth){

                     # Obtengo subdirectorios de la carpeta madre
                    $subfolders = Get-ChildItem $drive -Recurse -ErrorVariable +err `
                    -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                    Select-Object -ExpandProperty FullName
               }
               
               

               Write-Progress -Activity "Leyendo Directorios..." -Status $drive -PercentComplete 100
               
                            

               # Validamos que tenga subdirectorios para evitar nulos
               if($subfolders){

                   # junto todo en la variable folders
                   $folders+= $subfolders
               }          
               
              # Creo un customobject usando la técnica typedeclaration (solo valido en v3 y v4)
              [pscustomobject]@{
                    CountFolders=$folders.count
                    TotalFolders=$folders
                    ErrorVariable=$err
               } 


               #contabilizo si tiene subfolder
               $row=2
               # Va a ser el contandor de las carpetas
               $j=0
               #$folderCount=$folders.count
               #Write-host "ERRORLOG: $err"
               #write-output "Cantidad de directorios $folderCount"
               
               


               <#                          
               # UNA VEZ OBTENIDO TODOS LOS SUBDIRECTORIOS
               # Leo todos los directorios recursivamente
               foreach ($folder in $folders){

                                        
               Write-Progress -Activity "Escaneando en directorios..." -CurrentOperation "Leyendo $folder" `
                   -Status "Por favor espere....[$j]/[$folderCount]" -PercentComplete ($j/$folderCount*100)
                                        
                                                
                 
               #Leo carpeta por carpeta los permisos
                        try{

                            # Obtengo la cantidad de caracteres
                            #$cantidad=get-StringLength($folder)
                            #seteo la cantidad de - según la cantidad de caracteres del $folder
                            #$str="-"*($cantidad+11)
                                                            
                                                            
                            #$permissions=get-FilePermissions $folder
                                                        

                            #Write-Output "Cantidad de elementos $($permissions.count)"
                            #Write-output "`n[Carpeta];$folder;`t$permissions" 
                            #Get-Acl $folder -ErrorAction Continue

                                                
                    
                        }catch{
                            # Creo un custom object
                            $err += New-Object -TypeName psobject -Property @{ErrorType=$_CategoryInfo;TargetObject=$folder}
                        }
                                
                   $j++
                     

        }# Cierre foreach
        #> 
        #return $objeto.Count,$objeto.Folders,$objeto.ErrorVariable
        #return $folderCount,$folders,$err
        #Write-Output "Carpetas: $objeto.Folders"
        #Write-Output "Cantidad Carpetas: $objeto.Count"
        #Write-Output "Errores: $objeto.ErrorVariable"
        #return $folders
}# Cierre PROCESS   

    
}  


