## get-Sfr
function get-Sfr{


    
    [CmdLetBinding()]
    param(
        [parameter(Mandatory=$true)]
        #[validateScript({
        #   if(-not (Test-Connection $_ -Count 1 -Quiet)){
        #    throw "No se encuentra el server solicitado"
        #   }
        #   else{
        #       $True
        #   }
       # })] # Cierre validateScript
        [String]$server,
        [parameter(Mandatory=$false)]
        [int]$depth

        
    )
    
    BEGIN {
        #$VerbosePreference = "continue"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\get-StringLength[OK].ps1" 
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\get-FilePermissions[OK].ps1"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\Get-HddDrives[OK].ps1"
        #. "C:\Temp\PF\Proyecto FileServer\Get-CredentialInput[OK].ps1"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\Get-CredentialInput[OK].ps1"
        #. "C:\Octavio\Personal\Powershell\Proyectos\Sfr\Get-AclLocaly[OK].ps1"
        
        # Esto significa que las exceptions de tipo "nonterminating", las transformo en "terminating", lo que posibilita
        # que pueda tratarlas con try/catch
        # NOTA: Nunca hacer $ErrorActionPreference = 'SilentlyContinue' .
        #       Porque continua ante los errores nonterminating y de esta manera no nos enteraríamos de los errores
        $ErrorActionPreference="stop"
        $array=@()
        $errorLog=@()
        $permissions=@()
        $driveArray=@()
        $folderCountArray=@()
        $ht=@{}
        $date=Get-Date -Format "dd-MM-yyyy_HHmm"
        $ARCHIVO="c:\temp\permisos_$server"+"_$date.csv"
        
        ## Primero Valido que el server exista y que la conexión remota esté habilitada
        #if (-not (get-TestPsRemoting $server)){
        #    return
        #}

        # Valido la clave
        try {
            # Obtengo la credencial
            $cred=Get-CredentialInput
            #creo Session
            $session=New-PSSession -ComputerName $server -Credential $cred -ErrorAction stop
            
            
        
        }catch{
            switch -wildcard ($_.FullyQualifiedErrorId) {
                
                'CannotConnect,PSSessionOpenFailed'{
                    Write-host "Hubo un problema en la validacion, verifique que el servicio winrm esté levantado" -ForegroundColor black -BackgroundColor yellow
                    #pause
                    #exit 0
                    
                }
                
                'AccessDenied,PSSessionOpenFailed*'{
                    Write-host "No posee los permisos para acceder al server" -ForegroundColor black -BackgroundColor yellow
                    #Pause
                    #exit 0
                    
                }    
                
            }
        }


        
    }#CIERRE BEGIN
    
    
    PROCESS {

       
        
        

        
    
        # Si no obtuve sesion termina
        if ($session){

                    # CREO EXCEL
                    Write-Output "Directory`tIdentity`tPermission`tApplyTo" >> $ARCHIVO
                    
                    # Obtengo drives del server
                    $drives=get-HddDrives $server $cred

                   
                    # Por cada drive
                    foreach($drive in $drives){
                        
                        $j=0

                        #$folderCount,$folders,$errorLog=Invoke-Command -Session $session -ScriptBlock ${function:get-AclLocally} -args $drive #-Credential $cred
                        #Write-Output "Voy a pasar los parametros $drive y $depth, que es de tipo $($depth.gettype())"
                        $results=Invoke-Command -Session $session -ScriptBlock ${function:get-AclLocally} -args $drive,$depth #-Credential $cred
                       

                        Write-Host "Cantidad de Folders= $($results.CountFolders)" -ForegroundColor black -BackgroundColor yellow
                        
                        $folderCount= $($results.CountFolders)

                        # Cuento para poner al final del excel                                   
                        $array+=@{
                            Directorio=$drive
                            CantidadFolders=$results.CountFolders
                        }

                        #Write-Output "Cantidad directorios: $($results.CountFolders)"
                        #Write-Output "Directorios: $($results.TotalFolders)"
                        #Write-Output "Errores: $($results.ErrorVariable)"
                                                
                        # por cada folder
                        foreach($folder in $($results.TotalFolders)){

                            Write-Host "Folder : $folder" -ForegroundColor Red
                            
                            #$folderFinal+=$folder
                            Write-Progress -Activity "Escaneando en directorios..." -CurrentOperation "Leyendo $folder" `
                            -Status "Por favor espere....[$j]/[$folderCount]" -PercentComplete ($j/$folderCount*100)

                                $permissions=Invoke-Command -Session $session -ScriptBlock ${function:get-FilePermissions} -args $folder #-Credential $cred
                                Write-Output "$permissions"
                                
                                #$permissions=get-FilePermissions $folder

                            # Agrego en Excel los permisos
                            for($i=0;$i -lt $permissions.count;$i++){
                            
                                        
                                        #Write-Output "$folder`t$($permissions[$i])" >> $ARCHIVO
                                        Write-Output ("$folder`t$($permissions[$i].Identity)`t"+ 
                                                    "$($permissions[$i].Permission.Value)`t"+
                                                    "$($permissions[$i].ApplyTo)") >> $ARCHIVO
                                      
                            }
                            
                            $j++
                        }

                    
                    # Por último guardo el resumen de drives y su cantidad de directorios
                    foreach($arr in $array){
                        Write-Output "Drive=$($arr.Directorio)`tCantidad Directorios=$($arr.CantidadFolders)" >> $ARCHIVO
                        
                    }
                   

                    $results.ErrorVariable | Select-Object TargetObject,CategoryInfo | Export-Csv "c:\temp\filepermissionErrors.csv" -NoTypeInformation
                     

         }# Cierre foreach drive           

        }# Cierre IF $session
         # Si no tuve sesion, se termina
    }# cierre PROCESS

}


## get-SfrLocal
function get-SfrLocal{



    
    [CmdLetBinding()]
    param(
        [parameter(Mandatory=$false)]
        #[validateScript({
        #   if(-not (Test-Connection $_ -Count 1 -Quiet)){
        #    throw "No se encuentra el server solicitado"
        #   }
        #   else{
        #       $True
        #   }
       # })] # Cierre validateScript
        [String]$server,
        [parameter(Mandatory=$false)]
        [int]$depth

        
    )
    
    BEGIN {
        #$VerbosePreference = "continue"
        #"C:\Octavio\Personal\Powershell\Proyectos\Sfr Local\get-StringLengthLocal[OK].ps1" 
        #"C:\Octavio\Personal\Powershell\Proyectos\Sfr Local\get-FilePermissionsLocal[OK].ps1"
        #"C:\Octavio\Personal\Powershell\Proyectos\Sfr Local\Get-HddDrivesLocal[OK].ps1"
        #"C:\Octavio\Personal\Powershell\Proyectos\Sfr Local\Get-CredentialInputLocal[OK].ps1"
        #"C:\Octavio\Personal\Powershell\Proyectos\Sfr Local\Get-AclLocalyLocal[OK].ps1"
        
        # Esto significa que las exceptions de tipo "nonterminating", las transformo en "terminating", lo que posibilita
        # que pueda tratarlas con try/catch
        # NOTA: Nunca hacer $ErrorActionPreference = 'SilentlyContinue' .
        #       Porque continua ante los errores nonterminating y de esta manera no nos enteraríamos de los errores
        $ErrorActionPreference="stop"
            ## Valido que sino pasé nombre del server por parámetro tome el hostname
            if (!$server){
                $server = hostname
            }
        
        $array=@()
        $errorLog=@()
        $permissions=@()
        $driveArray=@()
        $ht=@{}
        $date=Get-Date -Format "dd-MM-yyyy_HHmm"
        $ARCHIVO="c:\temp\permisos_$server"+"_$date.csv"
        
        ## Primero Valido que el server exista y que la conexión remota esté habilitada
        #if (-not (get-TestPsRemoting $server)){
        #    return
        #}

        # Valido la clave
        <#
            try {
            # Obtengo la credencial
            $cred=Get-CredentialInput
            #creo Session
            $session=New-PSSession -ComputerName $server -Credential $cred -ErrorAction stop
            
            
        
        }catch{
            switch -wildcard ($_.FullyQualifiedErrorId) {
                
                'CannotConnect,PSSessionOpenFailed'{
                    Write-host "Hubo un problema en la validacion, verifique que el servicio winrm esté levantado" -ForegroundColor black -BackgroundColor yellow
                    #pause
                    #exit 0
                    
                }
                
                'AccessDenied,PSSessionOpenFailed*'{
                    Write-host "No posee los permisos para acceder al server" -ForegroundColor black -BackgroundColor yellow
                    #Pause
                    #exit 0
                    
                }    
                
            }
        }
        #>

        
        }#CIERRE BEGIN
    
    
    PROCESS {

       
        
        

        
    
                    # CREO EXCEL
                    Write-Output "Directory`tSecurity" >> $ARCHIVO
                    
                    # Obtengo drives del server
                    $drives=get-HddDrivesLocal $server

                   
                    # Por cada drive
                    foreach($drive in $drives){
                        
                        $j=0

                        #$results.CountFolders,$folders,$errorLog=Invoke-Command -Session $session -ScriptBlock ${function:get-AclLocally} -args $drive #-Credential $cred
                        #Write-Output "Voy a pasar los parametros $drive y $depth, que es de tipo $($depth.gettype())"
                        $results=get-AclLocallyLocal -drive $drive -depth $depth
                        

                        Write-Host "Cantidad de Folders= $($results.CountFolders)" -ForegroundColor black -BackgroundColor yellow
                        
                        $results.CountFolders= $($results.CountFolders)
                                                           
                        $array+=@{
                            Directorio=$drive
                            CantidadFolders=$results.CountFolders
                        }

                        #Write-Output "Cantidad directorios: $($results.CountFolders)"
                        #Write-Output "Directorios: $($results.TotalFolders)"
                        #Write-Output "Errores: $($results.ErrorVariable)"
                                                
                        # por cada folder
                        foreach($folder in $($results.TotalFolders)){

                            Write-Host "Folder : $folder" -ForegroundColor Red
                            
                            #$folderFinal+=$folder
                            Write-Progress -Activity "Escaneando en directorios..." -CurrentOperation "Leyendo $folder" `
                            -Status "Por favor espere....[$j]/[$results.CountFolders]" -PercentComplete ($j/$results.CountFolders*100)

                                $permissions+=get-FilePermissionsLocal <#-args#> $folder #-Credential $cred
                                Write-Output "$folder"
                                #$permissions=get-FilePermissions $folder

                            # Agrego en Excel los permisos
                            <#for($i=0;$i -lt $permissions.count;$i++){
                            
                                        
                                        Write-Output "$folder`t$($permissions[$i])" >> $ARCHIVO
                                      
                            }
                            
                            $j++
                            #>
                            
                            }
                                                
                      
                       
                       
                    } # Cierre ForEach drives     
                   

                <#        
                        
                        $errorLog=@()
                        Write-Progress -Activity "Leyendo Directorios..." -Status $path -PercentComplete 0 
                                                
                        # Creo array de carpeta 
                        $folders=@()

                        # Obtengo nombre de la carpeta que pase por parametro
                        $folders +=Get-Item $path | Select-Object -ExpandProperty FullName
                                                
                        # Obtengo subdirectorios de la carpeta madre
                        $subfolders = Get-ChildItem $path -Recurse -ErrorVariable +ErrorLog `
                        -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                        Select-Object -ExpandProperty FullName
                        Write-Progress -Activity "Leyendo Directorios..." -Status $path -PercentComplete 100

                        # Validamos que tenga subdirectorios para evitar nulos
                        if($subfolders){

                            # junto todo en la variable folders
                            $folders += $subfolders
                        }          
                                                
                        #contabilizo si tiene subfolder
                        $row=2
                        # Va a ser el contandor de las carpetas
                        $j=0
                        $results.CountFolders=$folders.count
                        write-output "Cantidad de directorios $results.CountFolders"
                                                    
                                                    

                        # Leo todos los directorios recursivamente
                        foreach ($folder in $folders){

                                                    
                        Write-Progress -Activity "Escaneando en directorios..." -CurrentOperation "Leyendo $folder" `
                            -Status "Por favor espere....[$j]/[$results.CountFolders]" -PercentComplete ($j/$results.CountFolders*100)
                                                    
                                                            

                        #Leo carpeta por carpeta los permisos
                        try{
                            # Obtengo la cantidad de caracteres
                            $cantidad=get-StringLength($folder)
                            #seteo la cantidad de - según la cantidad de caracteres del $folder
                            $str="-"*($cantidad+11)
                                                            
                                                            
                            $permissions=get-FilePermissions $folder
                                                        

                            #Write-Output "Cantidad de elementos $($permissions.count)"
                            #Write-output "`n[Carpeta];$folder;`t$permissions" 
                            $acl = Get-Acl -LiteralPath $folder -ErrorAction Continue
                                                            
                            # Agrego en Excel los permisos
                                                            
                            for($i=1;$i -lt $permissions.count;$i++){
                            $c.cells.item($row,$columnName) = $folder
                            $c.cells.item($row,$columnSecurity) = $permissions[$i]
                            $row++
                            }
                                                            
                                                            

                        }catch{
                            # Creo un custom object
                            $errorLog += New-Object -TypeName psobject -Property @{ErrorType=$_CategoryInfo;TargetObject=$folder}
                        }
                                        
                            $j++
                                                
                                                
                                
                                # Voy mostrando los permisos sobre cada carpeta
                                $acl.Access | ? {$_.IsInherited  -eq $false} | 
                                Select-Object `
                                @{n='Root';e={$path}},`
                                @{n='Path';e={$folder}},`
                                @{n='Group or User';e={$_.identityreference}},`
                                @{n='Permissions';e={$_.filesystemrights}},`
                                @{n='Inherited';e={$_.isinherited}},`
                                inheritanceFlags,propagationflags
                                    
                                                    
                                                    
                                                    

                        } # cierre foreach


                    }


                
                    # Agrego Titulo
                    $c.cells.item(1,$columnTitleName) = "Directory Name"
                    $c.cells.item(1,$columnTitleSecurity) = "Security"
                    $b.SaveAs("c:\temp\permisos.xls")
                    $a.quit()

                     #>

                    # Exporto los errores que vengo capturando en la obtencion de subdirectorios y acls
                                        #$errorLog | Select-Object ErrorType, TargetObject | Export-Csv "c:\temp\filepermission.csv" -NoTypeInformation
                    #Write-Output "CARPETA:$($errorlog.folders)`t`tERROR:$($errorlog.ErrorVariable.exception)"
                    
                    # Por último guardo el resumen de drives y su cantidad de directorios
                    <#foreach($arr in $array){
                        Write-Output "Drive=$($arr.Directorio)`tCantidad Directorios=$($arr.CantidadFolders)" >> $ARCHIVO
                    }
                    

                    $results.ErrorVariable | Select-Object TargetObject,CategoryInfo | Export-Csv "c:\temp\filepermissionErrors.csv" -NoTypeInformation
                    #>
                    foreach ($perm in $permissions){
                                $folder + "`t" + $perm.permission

                    }

       
    }# cierre PROCESS

}


## Get-AclLocaly
function Get-AclLocally {

    [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [String]$drive,
        [Parameter(Mandatory=$false)]
        [int]$depth 

    )

    BEGIN{
        #$err=@()
        $simbolo='*\'
        $dirArray=@()
        # Creo array de carpeta 
        $folders=@()
        # Creo array de subcarpeta
        $subfolders=@()
        $ultimaLetra=$drive.Substring($drive.length-1)
        
        #Si la ultima letra no contiene '\' se la agrega
        if(-not ($ultimaLetra.Contains(('\')))){
            $drive+='\'
        }
        
    }
            

    PROCESS {
               
               Write-Progress -Activity "Leyendo Directorios..." -Status $drive -PercentComplete 0 
                                    
               
               

               # Obtengo nombre de la carpeta que pase por parametro
               $folders +=Get-Item $drive | Select-Object -ExpandProperty FullName

               # Si recibe profundidad de subdirectorios                     
               if ($depth){

                    $tempDepth=""

                    # Por cada profundidad
                    foreach ($i in 1..$depth){
                        
                        
                        $tempDepth+=$simbolo
                        $dirArray+=$tempDepth   

                       
                    }                

                    # En dirArray tengo el string del depth (ej: "\\\ 3 niveles"),
                    # o sea, tengo la cantidad de niveles que tengo que recorrer
                    foreach ($j in $dirArray){

                        
                        Write-Output "drive= $($drive+$j)"
                        # Por cada directorio que recorro (ej: d:\*\). pregunto si tiene subdirectorios
                        # Si tiene subdirectorios, entonces listo los subdirectorios con los permisos
                        # Sino, break
                        # Esto es para 
                        if (Get-ChildItem $drive$j -ErrorAction SilentlyContinue -ErrorVariable +err | 
                            ? {$_.psiscontainer -eq $true})
                            {
                                    $subfolders+= Get-ChildItem $drive$j -ErrorVariable +err `
                                    -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                                    Select-Object -ExpandProperty FullName
                            }
                          else{
                              break
                          }  
                        
                    }
                    

               }
               elseif (-not $depth){

                     # Obtengo subdirectorios de la carpeta madre
                    $subfolders = Get-ChildItem $drive -Recurse -ErrorVariable +err `
                    -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                    Select-Object -ExpandProperty FullName
               }
               
               

               Write-Progress -Activity "Leyendo Directorios..." -Status $drive -PercentComplete 100
               
                            

               # Validamos que tenga subdirectorios para evitar nulos
               if($subfolders){

                   # junto todo en la variable folders
                   $folders+= $subfolders
               }          
               
              # Creo un customobject usando la técnica typedeclaration (solo valido en v3 y v4)
              [pscustomobject]@{
                    CountFolders=$folders.count
                    TotalFolders=$folders
                    ErrorVariable=$err
               } 


               #contabilizo si tiene subfolder
               $row=2
               # Va a ser el contandor de las carpetas
               $j=0
               #$results.CountFolders=$folders.count
               #Write-host "ERRORLOG: $err"
               #write-output "Cantidad de directorios $results.CountFolders"
               
               


               <#                          
               # UNA VEZ OBTENIDO TODOS LOS SUBDIRECTORIOS
               # Leo todos los directorios recursivamente
               foreach ($folder in $folders){

                                        
               Write-Progress -Activity "Escaneando en directorios..." -CurrentOperation "Leyendo $folder" `
                   -Status "Por favor espere....[$j]/[$results.CountFolders]" -PercentComplete ($j/$results.CountFolders*100)
                                        
                                                
                 
               #Leo carpeta por carpeta los permisos
                        try{

                            # Obtengo la cantidad de caracteres
                            #$cantidad=get-StringLength($folder)
                            #seteo la cantidad de - según la cantidad de caracteres del $folder
                            #$str="-"*($cantidad+11)
                                                            
                                                            
                            #$permissions=get-FilePermissions $folder
                                                        

                            #Write-Output "Cantidad de elementos $($permissions.count)"
                            #Write-output "`n[Carpeta];$folder;`t$permissions" 
                            #Get-Acl $folder -ErrorAction Continue

                                                
                    
                        }catch{
                            # Creo un custom object
                            $err += New-Object -TypeName psobject -Property @{ErrorType=$_CategoryInfo;TargetObject=$folder}
                        }
                                
                   $j++
                     

        }# Cierre foreach
        #> 
        #return $objeto.Count,$objeto.Folders,$objeto.ErrorVariable
        #return $results.CountFolders,$folders,$err
        #Write-Output "Carpetas: $objeto.Folders"
        #Write-Output "Cantidad Carpetas: $objeto.Count"
        #Write-Output "Errores: $objeto.ErrorVariable"
        #return $folders
}# Cierre PROCESS   

    
}  


## Get-AclLocallyLocal
function Get-AclLocallyLocal {

     [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$true)]
        [String]$drive,
        [Parameter(Mandatory=$false)]
        [int]$depth 

    )

    BEGIN{
        #$err=@()
        $simbolo='*\'
        $dirArray=@()
        # Creo array de carpeta 
        $folders=@()
        # Creo array de subcarpeta
        $subfolders=@()
        $ultimaLetra=$drive.Substring($drive.length-1)
        
        #Si la ultima letra no contiene '\' se la agrega
        if(-not ($ultimaLetra.Contains(('\')))){
            $drive+='\'
        }
        
    }
            

    PROCESS {
               
               Write-Progress -Activity "Leyendo Directorios..." -Status $drive -PercentComplete 0 
                                    
               
               

               # Obtengo nombre de la carpeta que pase por parametro
               $folders +=Get-Item $drive | Select-Object -ExpandProperty FullName

               # Si recibe profundidad de subdirectorios                     
               if ($depth){

                    $tempDepth=""

                    # Por cada profundidad
                    foreach ($i in 1..$depth){
                        
                        
                        $tempDepth+=$simbolo
                        $dirArray+=$tempDepth   

                       
                    }                

                    # En dirArray tengo el string del depth (ej: "\\\ 3 niveles"),
                    # o sea, tengo la cantidad de niveles que tengo que recorrer
                    foreach ($j in $dirArray){

                        
                        # Write-Output "drive= $($drive+$j)"
                        # Por cada directorio que recorro (ej: d:\*\). pregunto si tiene subdirectorios
                        # Si tiene subdirectorios, entonces listo los subdirectorios con los permisos
                        # Sino, break
                        # Esto es para 
                        if (Get-ChildItem $drive$j -ErrorAction SilentlyContinue -ErrorVariable +err | 
                            ? {$_.psiscontainer -eq $true})
                            {
                                    $subfolders+= Get-ChildItem $drive$j -ErrorVariable +err `
                                    -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                                    Select-Object -ExpandProperty FullName
                            }
                          else{
                              break
                          }  
                        
                    }
                    

               }
               elseif (-not $depth){

                     # Obtengo subdirectorios de la carpeta madre
                    $subfolders = Get-ChildItem $drive -Recurse -ErrorVariable +err `
                    -ErrorAction SilentlyContinue | ? {$_.psiscontainer -eq $true} | 
                    Select-Object -ExpandProperty FullName
               }
               
               

               Write-Progress -Activity "Leyendo Directorios..." -Status $drive -PercentComplete 100
               
                            

               # Validamos que tenga subdirectorios para evitar nulos
               if($subfolders){

                   # junto todo en la variable folders
                   $folders+= $subfolders
               }          
               
              # Creo un customobject usando la técnica typedeclaration (solo valido en v3 y v4)
               <#
			   $hashtTable=@{
                    CountFolders=$folders.count
                    TotalFolders=$folders
                    ErrorVariable=$err
               } 
				
				$obj = 1 | select-object CountFolders,TotalFolders,ErrorVariable
				$obj.CountFolders = $folders.count
				$obj.TotalFolders = $folders
				$obj.ErrorVariable = $err
				
				$obj
				#>
				$hashTable=@{
                    CountFolders=$folders.count
                    TotalFolders=$folders
                    ErrorVariable=$err
				}
				
				return new-object -TypeName psobject -property $hashTable
}# Cierre PROCESS   

    
}  



## Get-CredentialInput
function get-CredentialInput{

    [CmdLetBinding()]
    param(
    [Parameter(Mandatory=$false)]
    [ValidateSet('nbk9qaus','roj48598s','nbkrqt8s','lfra2477s','rhr87744s','bm113150s','fa110791s', 
                'qv110627s','sg115382s','secguard')]
    [ValidateScript({

                try {
                    $user=$_
                    get-aduser -Identity $user -ErrorAction stop
                    $true
                }
                catch {
                    throw "No existe el usuario $user"
                }

    })]
    [String]$user,
    [Parameter(Mandatory=$false)]
    [String]$password
    )

   

    BEGIN{
        
        $domain="ardp"
        # Si no ingresé usuario por parámetro
        if(!$user){
            $Puser=Read-Host "Ingrese usuario "
            
        }
        if(!$password){
        
            $Ppassword=Read-Host "Ingrese password " -AsSecureString
            #$credential=New-Object System.Management.Automation.PSCredential("$domain\$user",$password)
        }
        elseif ($user -and $password){
            $Puser=$user
            $Ppassword = $password | ConvertTo-SecureString -AsPlainText -Force

        }    
        

     
      
      $credential=New-Object System.Management.Automation.PSCredential("$domain\$Puser",$Ppassword)

    }

    PROCESS{}


    END{
        return $credential
    }

}





## get-FilePermissions
function get-FilePermissions{

    [CmdletBinding()]
    param(# Parameter help description
    [Parameter(Mandatory=$true)]
    [ValidateScript({test-path -path $_})]
    [String]$path
    )

    BEGIN{
        $outputPreference = "continue"
        $array=@()
        
    }

    PROCESS{

            $acl = get-acl $path 
            
                   
            $acl.Access | % {
        
                #$grupo=$_.identityreference
                
                switch ($_) {
                   {(($_.InheritanceFlags -eq "none") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}     
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This Folder Only"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This Folder Only"
                        }

                   {(($_.InheritanceFlags -eq "ContainerInherit, ObjectInherit") -and `
                                                                ($_.PropagationFlags -eq "none") -and ($_.accesscontroltype -eq "allow"))}
                        {
                            
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder, sub-folders and files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This Folder Only"
                        
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder, sub-folders and files"
                        }

                    {(($_.InheritanceFlags -eq "ContainerInherit") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder and sub-folders"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder and sub-folders"
                        }
                    {(($_.InheritanceFlags -eq "ObjectInherit") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder and files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder and files"
                        }
                    {(($_.InheritanceFlags -eq "ContainerInherit, ObjectInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Subfolders and file"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Subfolders and files"
                        }
                    {(($_.InheritanceFlags -eq "ContainerInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Subfolders"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Subfolders"
                        }
                    {(($_.InheritanceFlags -eq "ObjectInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and`
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Files"
                        }           
                   Default {}        
                } #cierre switch
      
            }# Cierre foreach 
        Write-Output $array
        }# cierre PROCESS
           
}# cierre Function


## get-FilePermissionsLocal
function get-FilePermissionsLocal{

 [CmdletBinding()]
    param(# Parameter help description
    [Parameter(Mandatory=$true)]
    #[ValidateScript({test-path -path '$_'})]
    [String]$path
    )

    BEGIN{
        $outputPreference = "continue"
        $array=@()
        
    }

    PROCESS{

            
      try{      
            
            $acl = get-acl $path -ErrorAction Stop
            
                   
            $acl.Access | % {
        
                #$grupo=$_.identityreference
                
                switch ($_) {
                   {(($_.InheritanceFlags -eq "none") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}     
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This Folder Only"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This Folder Only"
                        }

                   {(($_.InheritanceFlags -eq "ContainerInherit, ObjectInherit") -and `
                                                                ($_.PropagationFlags -eq "none") -and ($_.accesscontroltype -eq "allow"))}
                        {
                            
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder, sub-folders and files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This Folder Only"
                        
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder, sub-folders and files"
                        }

                    {(($_.InheritanceFlags -eq "ContainerInherit") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder and sub-folders"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder and sub-folders"
                        }
                    {(($_.InheritanceFlags -eq "ObjectInherit") -and ($_.PropagationFlags -eq "none") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="This folder and files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];This folder and files"
                        }
                    {(($_.InheritanceFlags -eq "ContainerInherit, ObjectInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Subfolders and file"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Subfolders and files"
                        }
                    {(($_.InheritanceFlags -eq "ContainerInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and `
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Subfolders"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Subfolders"
                        }
                    {(($_.InheritanceFlags -eq "ObjectInherit") -and ($_.PropagationFlags -eq "InheritOnly") -and`
                                                                ($_.accesscontroltype -eq "allow"))}            
                        {
                            $prop =     @{Identity = $_.IdentityReference.toString()
                                        Permission = $_.FileSystemRights
                                        ApplyTo ="Files"
                                        }
                            $entry = New-Object -TypeName psobject -Property $prop
                            $array+=$entry 
                            #Write-output "[$($_.IdentityReference)];$($_.FileSystemRights);[Apply To];Files"
                        }           
                   Default {}        
                } #cierre switch
      
            }# Cierre foreach 
      }# Cierre Try
    catch{
        $prop   =   @{Path=$path
                    Error=$_.Exception.Message
                    }
        $entry = New-Object -TypeName psobject -Property $prop
        $array+=$entry
        }
    finally
    {
        Write-Output $array
    }
          
        }# cierre PROCESS
           
}# cierre Function


## get-HddDrives
function get-HddDrives {

    [CmdletBinding()]
    param (
        
        [Parameter(Mandatory=$true)][String]$server,
        [Parameter(Mandatory=$true)][System.Management.Automation.PSCredential]$cred
    )

    BEGIN{
            <#
            $DOMAIN = "ardp"
            #$usuario=Read-Host "Usuario: "
            #$password=Read-Host "Password: " -AsSecureString
            $usuario="roj48598s"
            $password=ConvertTo-SecureString -String "Zaqwsx12" -AsPlainText -force
            $credential = New-Object  System.Management.Automation.PsCredential("$DOMAIN\$usuario",$password)
            $server=$item
            $unidad=""
            $label=""

            #$drives=[System.IO.DriveInfo]::GetDrives()
            #>        
    }

    PROCESS{
       
        
            if($server -ne "localhost"){


            
                    if (Test-WSMan $server -ErrorAction Ignore){
                    
                    
                        Invoke-Command $server {[System.IO.DriveInfo]::GetDrives() | 
                                #foreach ($_.drivetype) {
                                foreach ($_){

                                    #if($_.DriveType -match "fixed" -and $_.name -notlike "c:\"){
                                   
                                    #Listo los drives exceptuando el C
                                    if($_.DriveType -match "fixed" -and $_.name -notmatch "c"){    
                                        
                                        write-output "$($_.RootDirectory)" #`t$($_.VolumeLabel)"
                                    }
                                    
                                }
                    }   -Credential $cred  

                    }    
                    else{
                        Write-Output "No se pudo conectar, o no existe el server"
                    }
            }
            elseif ($server -eq "localhost") {
                        [System.IO.DriveInfo]::GetDrives() | 
                             foreach ($_.drivetype) {
                                  if($_.DriveType -match "fixed"){
                                
                                        write-output "$($_.RootDirectory)" #`t$($_.VolumeLabel)"
                                    }               
                             }
                                      
            }
        
		
        
         <#
         Write-Output "c:\windows\*\*"
		 #Write-Output "f:\"   
         #Write-Output "g:\" 
         #Write-Output "h:\"
         #>
               
    }# Cierre PROCESS

  
}# Cierre Function


## get-HddDrivesLocal
function get-HddDrivesLocal {

    [CmdletBinding()]
    param (
        
        [Parameter(Mandatory=$false)][String]$server,
        [Parameter(Mandatory=$false)][System.Management.Automation.PSCredential]$cred
    )

    BEGIN{
            <#
            $DOMAIN = "ardp"
            #$usuario=Read-Host "Usuario: "
            #$password=Read-Host "Password: " -AsSecureString
            $usuario="roj48598s"
            $password=ConvertTo-SecureString -String "Zaqwsx12" -AsPlainText -force
            $credential = New-Object  System.Management.Automation.PsCredential("$DOMAIN\$usuario",$password)
            $server=$item
            $unidad=""
            $label=""

            #$drives=[System.IO.DriveInfo]::GetDrives()
            #>        
    }

    PROCESS{
     
                        [System.IO.DriveInfo]::GetDrives() | 
                                #foreach ($_.drivetype) {
                                foreach ($_){

                                    #if($_.DriveType -match "fixed" -and $_.name -notlike "c:\"){
                                   
                                    #Listo los drives exceptuando el C
                                    if($_.DriveType -match "fixed"){# -and $_.name -notmatch "c"){    
                                        
                                        write-output "$($_.RootDirectory)" #`t$($_.VolumeLabel)"
                                    }
                                    
                                }
                    
                   
                                                
            }
        
		
        
         <#
         Write-Output "c:\windows\*\*"
		 #Write-Output "f:\"   
         #Write-Output "g:\" 
         #Write-Output "h:\"
         #>


  
}# Cierre Function




## get-StringLength
function get-stringLength(){

    [CmdLetBinding()]
    param(
        [parameter(Mandatory=$true)]
        [String]$path

    )

    BEGIN{}

    PROCESS{

        $cant=$path | Measure-Object -Character
        $cantidad=$cant.Characters
        return $cantidad
    }



}


## No-Lock
function set-noLock{

    [CmdLetBinding()]
    param(
        [Parameter(Mandatory=$false)]
        [int]$minutes=50
    )
    BEGIN{


            $myShell=New-Object -ComObject "wscript.Shell"
    }
    PROCESS{

        for($i=0;$i -lt $minutes;$i++){

            Start-Sleep -Seconds 60
            $myShell.Sendkeys(".")
            #Write-Output "Pasada: $i"
        }       
       
    }
}


## get-WhereLockedUsers
function get-WhereLockedUsers {

    [CmdLetBinding()]
    param(
       
        [Parameter(Mandatory=$false)]
        [ValidateScript({

                try {
                    $user=$_
                    get-aduser -Identity $user -ErrorAction stop
                    $true
                
                }catch {
                    throw "No existe en el dominio el usuario $user"
                }
              

        })] # Cierre ValidateScript
        [String]$user

    )


    BEGIN{
        $domainController="arpads003"
        $logname="Security"
        $id=4740
        $cred=get-CredentialInput
        $resultado=@()
        $titulo1="When was locked?"
        $titulo2="User Id"
        $titulo3="From where is it blocked?"

        $cantidadTitulo1=get-stringLength($titulo1)
        $cantidadTitulo2=get-stringLength($titulo2)
        $cantidadTitulo3=get-stringLength($titulo3)

        $strCantidadTitulo1 = "-" * $cantidadTitulo1
        $strCantidadTitulo2 = "-" * $cantidadTitulo2
        $strCantidadTitulo3 = "-" * $cantidadTitulo3
    }


    PROCESS{

        # Si no ingreso usuario
        if(-not($user)){


        try {    


                Get-WinEvent -ComputerName $domainController -FilterHashtable @{logname=$logname;id=$id} -Credential $cred -ErrorAction Stop | 
                    Select-Object @{n="When was locked?";e="timecreated"}, @{n="User Id";e={$_.properties[0].value}}, `
                        @{n="From where is it blocked?";e={$_.properties[1].value}}
            }catch {

                switch -Wildcard ($_.FullyQualifiedErrorId){

                    '*NoMatchingEventsFound*'{

                       Write-Host "No se encontraron resultados " -ForegroundColor red 
                    }


                } # Cierre switch
            }
            


        }

        # Si ingreso usuario
        elseif ($user) {
            
            try {

                $resultado=Get-WinEvent -ComputerName $domainController -FilterHashtable @{logname=$logname;id=$id;data=$user} `
                -Credential $cred -ErrorAction stop | Select-Object @{n="When was locked?";e="timecreated"}, `
                    @{n="User Id";e={$_.properties[0].value}}, `
                        @{n="From where is it blocked?";e={$_.properties[1].value}} 
                
                Write-Host "`n$titulo1`t$titulo2`t`t$titulo3`n$strCantidadTitulo1`t$strCantidadTitulo2`t`t$strCantidadTitulo3"
                foreach($usuario in $resultado){
                    Write-Host "$($usuario."When was locked?")`t$($usuario."User Id")`t$($usuario."From where is it blocked?")" -ForegroundColor Green
                }
                
               
            }catch{
                Write-Host "El usuario no tiene bloqueos registrados" -ForegroundColor Yellow
            }
            
            <#
            
            # Si no encuentra resultados para ese usuario
            if(!$resultado){
                Write-Host "El usuario no tiene bloqueos registrados" -ForegroundColor Yellow
            }
            elseif($resultado){
                Write-Host "$resultado" -ForegroundColor Green
            }
            #>
        } #Cierre elseif $user


       

    } # Cierre PROCESS


    END{}




}

## get-TestPsRemoting
function get-TestPsRemoting{

    [CmdLetBinding()]
    param([Parameter(Mandatory=$true)]
    [ValidateScript({

           if ( -not (test-connection -computername $_ -count 1 -quiet)){

                    throw "No se encuentra o esta caido el server $_"
           }
           else {
               $true
           }
    })]
    [String]$server

    )


    BEGIN{
            $cred = get-CredentialInput 
            try{
                $result=Invoke-Command -ComputerName $server {1} -Credential $cred -ErrorAction Stop
                $respuesta = $true
                #return $true
            }catch{

                $motivoError=$_.Exception.Message

                switch -Wildcard ($_.FullyQualifiedErrorId) {
                    
                    'AccessDenied,PSSessionStateBroken' {

                            Write-Host "Error!!. Acceso denegado`nMotivo: $motivoError" -BackgroundColor Yellow -ForegroundColor Black
                            $respuesta = $false
                            #return $false
                            
                    }

                    'CannotConnect,PSSessionStateBroken'{

                            Write-Host "Error!!. Acceso remoto al server $server deshabilitado
                                    `nMotivo: $motivoError)" -BackgroundColor Yellow -ForegroundColor Black
                            $respuesta = $false
                            #return $false
                            
                     }

                    Default {}
                }

                

                return $respuesta
                
                
            }   # Cierre Catch

            

    }

    PROCESS{}

    END{}

}


## get-directorySize
function get-DirectorySize {

    [CmdLetBinding()]
    param(
    [Parameter(Mandatory=$true)]
    [ValidateScript({test-path -path $_})]
    [String]$directory
    )

    BEGIN{}

    PROCESS{

                Get-ChildItem $directory | ? {$_.psiscontainer -eq $true} | foreach {

                    $subDir = Get-ChildItem $_.FullName -Recurse | ? {$_.psiscontainer -eq $true} | Measure-Object 
                    $subFiles = Get-ChildItem $_.FullName -Recurse | ? {$_.psiscontainer -eq $false}  | Measure-Object -Property Length -Sum
                    $_ | Add-Member -MemberType NoteProperty -Name 'SubDirectories' -Value $subDir.Count
                    $_ | Add-Member -MemberType NoteProperty -Name 'Files' -Value $subFiles.Count
                    $_ | Add-Member -MemberType NoteProperty -Name 'Size' -Value $subFiles.sum -PassThru
                    
                    
                } | sort size -descending | Select-Object @{n="Directory";e={$_.name}},SubDirectories,Files,@{n="Size (MB)";e={"{0:N3}" -f ($_.size/1mb)}}
                
                

          }

    

    END{}


} 

